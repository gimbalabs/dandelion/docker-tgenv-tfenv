FROM jarylc/tgenv-tfenv-alpine

ENV PATH=$PATH:/root/.tgenv/bin:/root/.tfenv/bin
WORKDIR /src
